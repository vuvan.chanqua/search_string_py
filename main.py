import time
start_time = time.time()

def bSearch(L, target):
    start = 0
    end = len(L) - 1
    while start <= end:
        middle = start + (end - start) // 2
        midpoint = L[middle]
        if midpoint == target:
            return middle
        elif midpoint > target:
            end = middle - 1
        else:
            start = middle + 1


def find4(dictionary):
    for index, line in enumerate(f, 1):
        dictionary.append(line.rstrip())
    dictionary = sorted(dictionary)
    s = input("Input search: ")
    out = bSearch(dictionary, s)
    if out is not None:
        print(s + " in line", out)
    else:
        print("No matching")


def find1(dictionary):
    s = input("Input search: ")
    check = False

    for index, line in enumerate(dictionary):
        if line.rstrip().lower() == s.rstrip().lower():
            print(line.rstrip() + " in line", index)
            check = True
    if not check:
        print("No matching")


def find2():
    f = open("word.txt", "r")
    dictionary = []
    for index, line in enumerate(f, 1):
        dictionary.append(line.rstrip())
    s = input("Input search: ")
    i = 0
    while True:
        dictionary1 = []
        for r in dictionary:
            if len(r) > i and len(s) > i:
                if r[i].lower() == s[i].lower():
                    dictionary1.append(r)

        i += 1
        if len(dictionary1) > 0:
            dictionary = dictionary1

        if len(dictionary1) == 0:
            break

    check = True
    for r in dictionary:
        if r.lower() == s.lower():
            print(r)
            check = False
    if check:
        print("No matching")


def find3():
    f = open("word.txt", "r")
    dictionary = {}
    dictionary_ind = {}
    for index, line in enumerate(f, 1):
        dictionary.setdefault(line.rstrip().lower(), line.rstrip())
        dictionary_ind.setdefault(line.rstrip().lower(), index)

    dictionary = dict(dictionary)
    s = input("Input search: ")
    if dictionary[s.lower()]:
        print(dictionary[s.lower()] + " in line", dictionary_ind[s.lower()])
    else:
        print("No matching")


def find5(dictionary1):
    data = [[] for _ in range(27)]
    for index, line in enumerate(dictionary1, 1):
        ck = ord(line.rstrip()[0].lower()) % 97
        if ck >= 0 and ck < 26:
            data[ck].append(line)
        else:
            data[26].append(line)

    s = input("Input search: ")
    fs = s[0].lower()
    index = len(data[26])-1
    if (ord(fs) - 97) > 0:
        for i in range(ord(fs) - 97):
            index += len(data[(ord(fs) - 97)])

    for r in data[(ord(fs) - 97)]:
        index = index + 1
        if r.rstrip().lower() == s.lower():
            print(r.rstrip() + " in line", index)
    #print("--- %s seconds ---" % (time.time() - start_time))

if __name__ == "__main__":
    f = open("word.txt", "r")
    dictionary = []
    for line in f:
        dictionary.append(line.rstrip())
    dictionary = sorted(dictionary)

    # Duyet tat ca phan tu mang string
    find1(dictionary)

    # tim kiem theo tung chu cai trong tu nhap
    #find2()

    # Dung dict trong python giong hashmap
    #find3()

    # Binary seach
    #find4(dictionary)

    # Chia thanh 26 mang chua 26 chu cai dau. sau do tim trong 26 mang do
    # find5(dictionary)
